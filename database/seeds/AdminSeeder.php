<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'type' => 'admin',
            'username' => 'masray',
            'password' => Hash:: make('qwe123'),
        ]);
        User::create([
            'type' => 'guru',
            'username' => 'uus',
            'password' => Hash:: make('qwe123'),
        ]);
        User::create([
            'type' => 'admin',
            'username' => 'admin',
            'password' => Hash:: make('123456'),
        ]);
        //
    }
}
